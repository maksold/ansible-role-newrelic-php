# Ansible Role: NewRelic PHP

Installs the Newrelic PHP Extension on RHEL/CentOS/Alma servers.

## Requirements

This role require Ansible 2.9 or higher.

This role was designed for:

- RHEL/CentOS/Alma 7/8/9

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    newrelic_license_key: ''

The API Key is required configuration to make Newrelic APM work.

    newrelic_php_appname: 'PHP Application'

The application name that data is reported under in APM. We highly recommends that you replace the default name with a
descriptive name to avoid confusion and unintended aggregation of data. Data for all applications with the same name
will be merged in New Relic, so set this carefully.

## Dependencies

No

## License

MIT / BSD

## Author Information

This role was created in 2021 by [Maksim Soldatjonok](https://www.maksold.com/).